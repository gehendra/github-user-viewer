(function () {
  var app = angular.module('githubViewer');
  var RepoController = function($scope, $routeParams, $log, github) {

    var reponame = $routeParams.reponame;
    var username = $routeParams.username;

    var onRepo = function(data) {
      $scope.repo = data;
    };

    var onError = function (error) {
      $scope.error = error;
    };

    github.getRepoDetails(username, reponame)
          .then(onRepo, onError);
  };
  app.controller("RepoController", RepoController)
}());
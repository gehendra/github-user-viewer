(function(){
  var app = angular.module('githubViewer');
  var MainController = function($scope, $interval, $location){

    var decrementCountdown = function(){
      $scope.countdown -= 1;
      if ($scope.countdown < 1)
        $scope.search();
    };

    var countInterval = null;
    var startCountdown = function(){
      countInterval = $interval(decrementCountdown, 1000, $scope.countdown);
    };
    
    $scope.search = function () {      
      if(countInterval){
        $interval.cancel(countInterval);
        $scope.countdown = null;
      }
      $location.path("/user/" + $scope.username);
      
    };
    $scope.username = "angular";
    $scope.countdown = 7;
    startCountdown();    
  };
  app.controller("MainController", MainController);

}());
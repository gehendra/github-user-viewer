(function(){
  
  var app = angular.module('githubViewer');
  var UserController = function($scope, github, $routeParams){
    var onUserComplete = function (data) {
      $scope.user = data;
      github.getRepos($scope.user).then(onRepos, onError);
    };

    var onError = function (reason) {
      $scope.error = "Couldn't fetch data";
    };
    
    var onRepos = function(data){
      $scope.repos = data;
    };

    $scope.username = $routeParams.username;
    $scope.repoSortOrder = "-stargazers_count";
    github.getUser($scope.username).then(onUserComplete, onError);
    $scope.countdown = 5;
  };

  app.controller("UserController", UserController);
  
}());